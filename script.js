var calculator = document.getElementById('calculator');
var display = document.getElementById("display");
var memory = 0;
var getValue = function (userInput) {
    display.value != null ? display.value += userInput : ' ';
};
var getResult = function () {
    return display.value;
};
var result = function () {
    try {
        var x = getResult();
        display.value = eval(x);
    }
    catch (_a) {
        display.value = "Syntax Error!";
    }
};
var F_E = function () {
    var value = getResult();
    var x = parseFloat(value).toExponential(10);
    display.value = x.toString();
};
var degtorad = function () {
    var value = getResult();
    display.value = (Number(value) * (Math.PI / 180)).toString();
};
var pi = function () {
    var value = getResult();
    var x = Number(value) * Math.PI;
    display.value = x.toString();
};
var e = function () {
    var value = getResult();
    var x = Number(value) * Math.E;
    display.value = x.toString();
};
var square = function () {
    var value = getResult();
    var x = Math.pow(Number(value), 2);
    display.value = x.toString();
};
var reciprocal = function () {
    var value = getResult();
    var x = Math.pow(Number(value), -1);
    display.value = x.toString();
};
var mod = function () {
    var value = getResult();
    var x = Math.abs(Number(value));
    display.value = x.toString();
};
var exp = function () {
    var value = getResult();
    var x = Math.exp(Number(value));
    display.value = x.toString();
};
var sqrt = function () {
    var value = getResult();
    var x = Math.sqrt(Number(value));
    display.value = x.toString();
};
var factorial = function () {
    var value = getResult();
    var fact = 1;
    for (var i = 1; i <= Number(value); i++) {
        fact = fact * i;
        display.value = fact.toString();
    }
};
var xpowOf10 = function () {
    var value = getResult();
    var x = Math.pow(10, Number(value));
    display.value = x.toString();
};
var log = function () {
    var value = getResult();
    var x = Math.log(Number(value));
    display.value = x.toString();
};
var ln = function () {
    var value = getResult();
    var x = Math.LN10 * Number(value);
    display.value = x.toString();
};
var deleteValue = function () {
    var length = display.value.length;
    display.value = display.value.substring(0, length - 1);
};
var sin = function () {
    var value = getResult();
    var x = Math.sin(Number(value));
    display.value = x.toString();
};
var cos = function () {
    var value = getResult();
    var x = Math.cos(Number(value));
    display.value = x.toString();
};
var tan = function () {
    var value = getResult();
    var x = Math.tan(Number(value));
    display.value = x.toString();
};
var abs = function () {
    var value = getResult();
    var x = Math.abs(Number(value));
    display.value = x.toString();
};
var asinh = function () {
    var value = getResult();
    var x = Math.asinh(Number(value));
    display.value = x.toString();
};
var acosh = function () {
    var value = getResult();
    var x = Math.acosh(Number(value));
    display.value = x.toString();
};
var atanh = function () {
    var value = getResult();
    var x = Math.asinh(Number(value));
    display.value = x.toString();
};
var MemoryStore = function () {
    var value = getResult();
    memory = Number(value);
    console.log(memory);
};
var MemoryClear = function () {
    display.value = "";
};
var MemoryRecall = function () {
    var value = getResult();
    display.value = memory.toString();
    console.log(value);
};
var MemoryPlus = function () {
    var value = getResult();
    memory = eval("".concat(memory, " + ").concat(value));
};
var MemoryMinus = function () {
    var value = getResult();
    memory = eval("".concat(memory, " - ").concat(value));
};
