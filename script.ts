const calculator = document.getElementById('calculator');
let display: HTMLInputElement = document.getElementById("display") as HTMLInputElement;
let memory: number = 0;

const getValue = (userInput: String): void => {
    display.value != null ? display.value += userInput : ' ';
}

const getResult = (): string => {
    return display.value;
}

const result = (): void => {
    try {
        let x = getResult();
        display.value = eval(x);
    } catch {
        display.value = "Syntax Error!"
    }
}

const F_E = (): void => {
    let value = getResult();
    let x = parseFloat(value).toExponential(10);
    display.value = x.toString();
}

const degtorad = () => {
    let value = getResult();
    display.value = (Number(value) * (Math.PI / 180)).toString()
}

const pi = (): void => {
    let value: string = getResult();
    let x: number = Number(value) * Math.PI;
    display.value = x.toString();
}

const e = (): void => {
    let value: string = getResult();
    let x: number = Number(value) * Math.E;
    display.value = x.toString();
}

const square = (): void => {
    let value: string = getResult();
    let x: number = Math.pow(Number(value), 2);
    display.value = x.toString()
}


const reciprocal = (): void => {
    let value: string = getResult();
    let x: number = Math.pow(Number(value), -1);
    display.value = x.toString()
}

const mod = (): void => {
    let value: string = getResult();
    let x: number = Math.abs(Number(value));
    display.value = x.toString()
}

const exp = (): void => {
    let value: string = getResult();
    let x: number = Math.exp(Number(value));
    display.value = x.toString()
}

const sqrt = (): void => {
    let value: string = getResult();
    let x: number = Math.sqrt(Number(value));
    display.value = x.toString()
}

const factorial = () => {
    let value: string = getResult();
    let fact: number = 1;
    for (let i: number = 1; i <= Number(value); i++) {
        fact = fact * i;
        display.value = fact.toString();
    }
}

const xpowOf10 = () => {
    let value: string = getResult();
    let x: number = Math.pow(10, Number(value));
    display.value = x.toString()
}

const log = () => {
    let value: string = getResult();
    let x: number = Math.log(Number(value));
    display.value = x.toString();
}

const ln = () => {
    let value: string = getResult();
    let x: number = Math.LN10 * Number(value);
    display.value = x.toString();
}

const deleteValue = (): void => {
    let length: number = display.value.length;
    display.value = display.value.substring(0, length - 1)
}
const sin = (): void => {
    let value: string = getResult();
    let x: number = Math.sin(Number(value));
    display.value = x.toString();
}
const cos = (): void => {
    let value: string = getResult();
    let x: number = Math.cos(Number(value));
    display.value = x.toString();
}
const tan = (): void => {
    let value: string = getResult();
    let x: number = Math.tan(Number(value));
    display.value = x.toString();
}

const abs = () => {
    let value: string = getResult();
    let x: number = Math.abs(Number(value));
    display.value = x.toString();
}

const asinh = () => {
    let value: string = getResult();
    let x: number = Math.asinh(Number(value));
    display.value = x.toString();
}

const acosh = () => {
    let value: string = getResult();
    let x: number = Math.acosh(Number(value));
    display.value = x.toString();
}

const atanh = () => {
    let value: string = getResult();
    let x: number = Math.asinh(Number(value));
    display.value = x.toString();
}


const MemoryStore = () => {
    let value: string = getResult();
    memory = Number(value);
    console.log(memory);

}

const MemoryClear = () => {
    display.value = ""
}

const MemoryRecall = () => {
    let value: string = getResult();
    display.value = memory.toString();
    console.log(value);
}

const MemoryPlus = () => {
    let value: string = getResult();
    memory = eval(`${memory} + ${value}`)
}
const MemoryMinus = () => {
    let value: string = getResult();
    memory = eval(`${memory} - ${value}`)
}